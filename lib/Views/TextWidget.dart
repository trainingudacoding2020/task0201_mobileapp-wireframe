import 'package:flutter/material.dart';

class AppBarText extends StatelessWidget {
  //***** Variable tampungan
  final String varText;

  //***** Parameter
  AppBarText({Key key, this.varText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      varText,
      style: TextStyle(
        color: Colors.white,
        fontSize: 15.0,
      ),
    );
  }
}
