import 'package:flutter/material.dart';

class AppConstants {
  static final String appName = "Udacoding App";
  static final Color selectedIcon = Colors.deepOrange;
  static final Color nonSelectedIcon = Colors.black;
}
