import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'My App Wireframe'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();

  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  String username = "", password = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _key,
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text('Login'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
              child: Column(
            children: <Widget>[
              TextFormField(
                controller: usernameController,
                decoration: InputDecoration(
                    fillColor: Colors.greenAccent, hintText: 'username'),
              ),
              SizedBox(
                height: 8.0,
              ),
              TextFormField(
                controller: passwordController,
                obscureText: true,
                decoration: InputDecoration(
                    fillColor: Colors.greenAccent, hintText: 'password'),
              ),
              SizedBox(
                height: 8.0,
              ),
              Container(
                color: Colors.green,
                child: MaterialButton(
                  child: Text("submit"),
                  textColor: Colors.white,
                  onPressed: () {
                    setState(() {
                      username = usernameController.text;
                      password = passwordController.text;
                    });
                  },
                ),
              ),
              SizedBox(
                height: 16.0,
              ),
              Text(username),
              Text(password),
            ],
          )),
        ));
  }
}
