import 'package:flutter/material.dart';

class PageAnimatedSwitcher extends StatefulWidget {
  PageAnimatedSwitcher({Key key}) : super(key: key);

  @override
  _PageAnimatedSwitcherState createState() => _PageAnimatedSwitcherState();
}

class _PageAnimatedSwitcherState extends State<PageAnimatedSwitcher> {
  bool isOn = false;

  Widget varMyWidget = Container(
    width: 200,
    height: 100,
    decoration: BoxDecoration(
        color: Colors.red,
        border: Border.all(
          color: Colors.black,
          width: 3,
        )),
  );

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          //>>>>>> Widget Animated Switcher
          AnimatedSwitcher(
            child: varMyWidget,
            //*** Set lamanya animasi berlangsung
            duration: Duration(seconds: 1),
            //*** Animasi nya
//            transitionBuilder: (child, animation) => ScaleTransition(
//              scale: animation,
//              child: child,
//            ),
            transitionBuilder: (child, animation) => RotationTransition(
              turns: animation,
              child: child,
            ),
          ),
          //>>>>>> Widget Switch On Off
          Switch(
            //*** Default value nya ada boolean
            value: isOn,
            //*** Onchange kasih parameter buat nilai baru nya
            onChanged: (varNewValue) {
              isOn = varNewValue;
              setState(() {
                //**** Method kalau set is on
                if (isOn == true) {
                  //*** Kalau beda container gak usah pakai key
//                  varMyWidget = SizedBox(
//                    width: 200,
//                    height: 100,
//                    child: Center(
//                      child: Text(
//                        "Switch : ON",
//                        style: TextStyle(
//                          color: Colors.green,
//                          fontWeight: FontWeight.bold,
//                          fontSize: 20,
//                        ),
//                      ),
//                    ),
//                  );

                  //*** Kalau sama container harus pakai key biar smooth
                  varMyWidget = Container(
                    key: ValueKey(1),
                    width: 200,
                    height: 100,
                    decoration: BoxDecoration(
                      color: Colors.green,
                      border: Border.all(color: Colors.black, width: 2),
                    ),
                  );
                } else {
                  varMyWidget = Container(
                    key: ValueKey(2),
                    width: 200,
                    height: 100,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      border: Border.all(color: Colors.black, width: 2),
                    ),
                  );
                }
              });
            },
            activeColor: Colors.green,
            inactiveTrackColor: Colors.red[200],
            inactiveThumbColor: Colors.red,
          ),
        ],
      ),
    );
  }
}
