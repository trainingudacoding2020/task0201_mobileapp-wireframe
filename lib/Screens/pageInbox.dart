import 'package:flutter/material.dart';

class PageInbox extends StatefulWidget {
  PageInbox({Key key}) : super(key: key);

  @override
  _PageInboxState createState() => _PageInboxState();
}

class _PageInboxState extends State<PageInbox> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text('Inbox Page'),
    );
  }
}
