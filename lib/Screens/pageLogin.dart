import 'package:appwireframe/Screens/pageHome.dart';
import 'package:appwireframe/Screens/pageRegistration.dart';
import 'package:flutter/material.dart';
import 'package:appwireframe/Helpers/appConstants.dart';
import 'package:fluttertoast/fluttertoast.dart';

class PageLogin extends StatefulWidget {
  static final String varRouteName = '/pageLoginRoute';

  PageLogin({Key key}) : super(key: key);

  @override
  _PageLoginState createState() => _PageLoginState();
}

class Dialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.black54,
                  children: <Widget>[
                    Center(
                      child: Column(children: [
                        CircularProgressIndicator(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Please Wait....",
                          style: TextStyle(color: Colors.blueAccent),
                        )
                      ]),
                    )
                  ]));
        });
  }
}

class _PageLoginState extends State<PageLogin> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  TextEditingController varUserName = TextEditingController();
  TextEditingController varPassword = TextEditingController();

  void _actionSignUp() {
    Navigator.pushNamed(context, PageRegistration.varRouteName);
  }

  Future<void> _handleSubmit(BuildContext context) async {
    try {
      //invoking login
      Dialogs.showLoadingDialog(context, _keyLoader);
      //await serivce.login(user.uid);

      await new Future.delayed(const Duration(seconds: 5), () {
        if ((varUserName.text == 'admin') && (varPassword.text == 'admin')) {
          // *** Disable Loader
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          Navigator.pushNamed(context, PageHome.varRouteName);
        } else {
          // *** Disable Loader
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          Fluttertoast.showToast(
            msg: 'User Name or Password is wrong.. !',
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            backgroundColor: Colors.red,
            timeInSecForIosWeb: 1,
          );
        }
      });
    } catch (error) {
      print(error);
    }
  }

  bool _hidden = true;

  _obSecure() {
    setState(() {
      _hidden = !_hidden;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(50, 100, 50, 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text('Welcome to ${AppConstants.appName} !',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 30,
                  ),
                  textAlign: TextAlign.center),
              Form(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 25.0),
                      child: TextFormField(
                        controller: varUserName,
                        decoration:
                            InputDecoration(labelText: 'User Name / Email'),
                        style: TextStyle(fontSize: 25.0),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 35.0),
                      child: TextFormField(
                        obscureText: _hidden,
                        controller: varPassword,
                        decoration: InputDecoration(
                          labelText: 'Password',
                          suffixIcon: IconButton(
                              icon: _hidden
                                  ? Icon(Icons.visibility_off)
                                  : Icon(Icons.visibility),
                              onPressed: _obSecure),
                        ),
                        style: TextStyle(fontSize: 25.0),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30.0),
                child: MaterialButton(
                  onPressed: () {
                    //_actionLogin();
                    _handleSubmit(context);
                  },
                  child: Text(
                    'Login',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25.0,
                      color: Colors.white,
                    ),
                  ),
                  color: Colors.blue,
                  height: MediaQuery.of(context).size.height / 12,
                  minWidth: double.infinity,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: MaterialButton(
                  onPressed: () {
                    _actionSignUp();
                  },
                  child: Text(
                    'Sign Up',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25.0,
                      color: Colors.white,
                    ),
                  ),
                  color: Colors.grey,
                  height: MediaQuery.of(context).size.height / 12,
                  minWidth: double.infinity,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
