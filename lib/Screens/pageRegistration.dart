import 'package:appwireframe/Screens/pageHome.dart';
import 'package:appwireframe/Views/TextWidget.dart';
import 'package:flutter/material.dart';
import 'package:appwireframe/Helpers/appConstants.dart';

class PageRegistration extends StatefulWidget {
  static final String varRouteName = '/pageRegistrationRoute';

  PageRegistration({Key key}) : super(key: key);

  @override
  _PageRegistrationState createState() => _PageRegistrationState();
}

class _PageRegistrationState extends State<PageRegistration> {
  void _actionSubmit() {
    Navigator.pushNamed(context, PageHome.varRouteName);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //** Tambah ini untuk bisa ada navigator nya
      appBar: AppBar(
        title: AppBarText(
          varText: 'Registration',
        ),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(50, 50, 50, 50),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text('Please enter the following information',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                    ),
                    textAlign: TextAlign.center),
                Form(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 25.0),
                        child: TextFormField(
                          decoration: InputDecoration(labelText: 'First Name'),
                          style: TextStyle(fontSize: 25.0),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 35.0),
                        child: TextFormField(
                          decoration: InputDecoration(labelText: 'Last Name'),
                          style: TextStyle(fontSize: 25.0),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 25.0),
                        child: TextFormField(
                          decoration: InputDecoration(labelText: 'City'),
                          style: TextStyle(fontSize: 25.0),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 35.0),
                        child: TextFormField(
                          decoration: InputDecoration(labelText: 'Country'),
                          style: TextStyle(fontSize: 25.0),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 35.0),
                        child: TextFormField(
                          decoration: InputDecoration(
                            labelText: 'Bio',
                          ),
                          style: TextStyle(
                            fontSize: 25.0,
                          ),
                          maxLines: 3,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 30.0),
                  child: MaterialButton(
                    onPressed: () {
                      _actionSubmit();
                    },
                    child: Text(
                      'Submit',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 25.0,
                        color: Colors.white,
                      ),
                    ),
                    color: Colors.blue,
                    height: MediaQuery.of(context).size.height / 12,
                    minWidth: double.infinity,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
