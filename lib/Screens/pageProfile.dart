import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class PageProfile extends StatefulWidget {
  PageProfile({Key key}) : super(key: key);

  @override
  _PageProfileState createState() => _PageProfileState();
}

class _PageProfileState extends State<PageProfile> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
      child: Column(
        //**** Diatas letaknya
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              MaterialButton(
                onPressed: () {},
                child: CircleAvatar(
                  backgroundImage: AssetImage('assets/images/Endiaz.jpg'),
//                  child: Image(
//                    image: AssetImage('assets/images/Endiaz.jpg'),
//                  ),
                  radius: MediaQuery.of(context).size.width / 10,
                ),
              ),
              Column(
                children: <Widget>[
                  AutoSizeText(
                    'Endiaz Ludia Arsanto',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                    ),
                  ),
                  AutoSizeText(
                    'arsantodiaz@gmail.com',
                    style: TextStyle(
                      fontSize: 8,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
