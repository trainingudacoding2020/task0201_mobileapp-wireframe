import 'package:flutter/material.dart';

class PageExplore extends StatefulWidget {
  PageExplore({Key key}) : super(key: key);

  @override
  _PageExploreState createState() => _PageExploreState();
}

class _PageExploreState extends State<PageExplore> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text('Explore Page'),
    );
  }
}
