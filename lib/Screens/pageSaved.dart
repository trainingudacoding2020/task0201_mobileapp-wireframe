import 'package:flutter/material.dart';

class PageSaved extends StatefulWidget {
  PageSaved({Key key}) : super(key: key);

  @override
  _PageSavedState createState() => _PageSavedState();
}

class _PageSavedState extends State<PageSaved> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text('Saved Page'),
    );
  }
}
