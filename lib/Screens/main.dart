import 'dart:async';
import 'package:appwireframe/Helpers/appConstants.dart';
import 'package:appwireframe/Screens/pageHome.dart';
import 'package:appwireframe/Screens/pageLogin.dart';
import 'package:appwireframe/Screens/pageRegistration.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

Future fetchStr() async {
  await new Future.delayed(const Duration(seconds: 5), () {});
  return 'Hello World';
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
      routes: {
        PageLogin.varRouteName: (context) => PageLogin(),
        PageRegistration.varRouteName: (context) => PageRegistration(),
        PageHome.varRouteName: (context) => PageHome(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //**** Buat Timer untuk ke login page
  @override
  void initState() {
    super.initState();
    startTime();
//    Timer(Duration(seconds: 5), () {
//      Navigator.pushNamed(context, PageLogin.varRouteName);
//    });
  }

  startTime() async {
    var duration = new Duration(seconds: 6);
    return new Timer(duration, doRoute);
  }

  doRoute() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => PageLogin()));
  }

  @override
  Widget build(BuildContext context) {
    Future fetchStr() async {
      await new Future.delayed(const Duration(seconds: 5), () {});
      return 'Hello World';
    }

    final Future str = fetchStr();

    return Scaffold(
      body: Center(
        child: FutureBuilder(
          future: str,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
//              return Text(snapshot.data);
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }

            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.account_balance,
                    size: 80,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Text(
                      "Welcome to ${AppConstants.appName}",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CircularProgressIndicator(),
                  ),
                ],
              ),
            );

            // By default, show a loading spinner
            return CircularProgressIndicator();
          },
        ),
      ),

//      body: Center(
//        child: Column(
//          mainAxisAlignment: MainAxisAlignment.center,
//          children: <Widget>[
//            Icon(
//              Icons.account_balance,
//              size: 80,
//            ),
//            Padding(
//              padding: const EdgeInsets.only(top: 20.0),
//              child: Text(
//                "Welcome to ${AppConstants.appName}",
//                style: TextStyle(
//                  fontWeight: FontWeight.bold,
//                  fontSize: 20,
//                ),
//                textAlign: TextAlign.center,
//              ),
//            ),
//            Padding(
//              padding: const EdgeInsets.all(10.0),
//              child: CircularProgressIndicator(
//                backgroundColor: Colors.green,
//                value: null,
//                strokeWidth: 2,
//              ),
//            ),
//          ],
//        ),
//      ),
    );
  }
}
