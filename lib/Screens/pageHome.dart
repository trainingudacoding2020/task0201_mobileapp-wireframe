import 'package:appwireframe/Screens/pageAnimatedSwitcher.dart';
import 'package:appwireframe/Screens/pageExplore.dart';
import 'package:appwireframe/Screens/pageInbox.dart';
import 'package:appwireframe/Screens/pageLogin.dart';
import 'package:appwireframe/Screens/pageProfile.dart';
import 'package:appwireframe/Screens/pageRegistration.dart';
import 'package:appwireframe/Screens/pageSaved.dart';
import 'package:appwireframe/Screens/pageTrips.dart';
import 'package:appwireframe/Views/TextWidget.dart';
import 'package:flutter/material.dart';
import 'package:appwireframe/Helpers/appConstants.dart';

class PageHome extends StatefulWidget {
  static final String varRouteName = '/pageHomeRoute';

  PageHome({Key key}) : super(key: key);

  @override
  _PageHomeState createState() => _PageHomeState();
}

class _PageHomeState extends State<PageHome> {
  int varSelectedIndex = 0;

  void actionDoLogout() {
    Navigator.pushNamed(context, PageLogin.varRouteName);
  }

  final List<String> varPageTitle = [
    'Animated',
//    'Explore',
    'Saved',
    'Trips',
    'Inbox',
    'Profile'
  ];

  //**** Buat List widget untuk halaman body
  final List<Widget> varWidgetPages = [
    PageAnimatedSwitcher(),
//    PageExplore(),
    PageSaved(),
    PageTrips(),
    PageInbox(),
    PageProfile()
  ];

  //Buat NavItem
  BottomNavigationBarItem actionBuildNavItem(
      int paramIndex, IconData paramIconData, String paramText) {
    return BottomNavigationBarItem(
      icon: Icon(
        paramIconData,
        color: AppConstants.nonSelectedIcon,
      ),
      activeIcon: Icon(
        paramIconData,
        color: AppConstants.selectedIcon,
      ),
      title: Text(
        paramText,
        style: TextStyle(
          color: varSelectedIndex == paramIndex
              ? AppConstants.selectedIcon
              : AppConstants.nonSelectedIcon,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: AppBarText(
          varText: varPageTitle[varSelectedIndex],
        ),
        //***** Disable back button
        automaticallyImplyLeading: false,
        centerTitle: false,
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  actionDoLogout();
                },
                child: Tooltip(
                  child: Icon(
                    Icons.cancel,
                    color: Colors.yellow,
                  ),
                  message: 'Log Out',
                ),
              )),
        ],
      ),
      body: varWidgetPages[varSelectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        //**** Buat saat dipilih, harus passing index
        onTap: (index) {
          setState(() {
            varSelectedIndex = index;
          });
        },
        currentIndex: varSelectedIndex,
        type: BottomNavigationBarType.fixed,

        //**** Tambahkan item di navigation bawah
        items: <BottomNavigationBarItem>[
          actionBuildNavItem(0, Icons.outlined_flag, varPageTitle[0]),
          actionBuildNavItem(1, Icons.favorite_border, varPageTitle[1]),
          actionBuildNavItem(2, Icons.hotel, varPageTitle[2]),
          actionBuildNavItem(3, Icons.message, varPageTitle[3]),
          actionBuildNavItem(4, Icons.person_outline, varPageTitle[4]),
        ],
      ),
    );
  }
}
